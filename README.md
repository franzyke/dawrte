# Quasar App

> A Quasar project

## Build Setup

`npm install -g quasar-cli`

``` bash
# install dependencies
$ npm install

# serve with hot reload at localhost:8080
$ quasar dev

# build for production with minification
$ quasar build

# lint code
$ quasar lint
```
