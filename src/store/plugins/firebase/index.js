import firebase from '../../../fb'

export default (store) => {
  store.subscribe((mutation, state) => {
    if (mutation.type === 'items/save') {
      let id = firebase.database().ref().child('items').push().key
      let item = JSON.parse(JSON.stringify(state.items.current))
      item.user = {
        id: state.auth.user.uid,
        phone: state.auth.user.phoneNumber,
        name: state.auth.user.displayName
      }
      item.date = new Date()
      firebase.database().ref('items/' + id).set(item, (error) => {
        if (error) {
          console.log(error)
        } else {
          console.log('saved')
          store.commit('items/saved')
        }
      })
    }
  })
}
