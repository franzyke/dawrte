const ns = 'auth/'

export default {
  [ns + 'user'] (state) {
    return state.user
  },
  [ns + 'busy'] (state) {
    return state.busy
  }
}
