const ns = 'auth/'

export default {
  [ns + 'setUser'] (state, user) {
    state.user = user
  },
  [ns + 'login'] (state) {
    state.busy = true
  },
  [ns + 'setBusy'] (state, payload) {
    state.busy = payload
  },
  [ns + 'loggedIn'] (state, user) {
    state.user = user
  }
}
