import items from './items'
import auth from './auth'

export default {
  items,
  auth
}
