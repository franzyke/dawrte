const ns = 'items/'

export default {
  [ns + 'all'] (state) {
    return state.all
  },
  [ns + 'current'] (state) {
    return state.current
  },
  [ns + 'busy'] (state) {
    return state.busy
  }
}
