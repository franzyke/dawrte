const ns = 'items/'

export default {
  [ns + 'setProperty'] (state, payload) {
    if (state.current === null) {
      state.current = {}
    }
    state.current[payload.property] = payload.value
  },
  [ns + 'fetchAll'] (state) {
    state.busy = true
  },
  [ns + 'receivedAll'] (state, payload) {
    state.all = payload
    state.busy = false
  },
  [ns + 'setCurrent'] (state, id) {
    state.current = state.all.find(i => i.id === id)
  },
  [ns + 'save'] (state) {
    state.busy = true
  },
  [ns + 'saved'] (state) {
    state.busy = false
    state.current = null
  }
}
