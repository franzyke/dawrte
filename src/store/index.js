import Vue from 'vue'
import Vuex from 'vuex'
Vue.use(Vuex)

import modules from './modules'
import plugins from './plugins'

export default new Vuex.Store({
  modules,
  plugins
})
