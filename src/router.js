import Vue from 'vue'
import VueRouter from 'vue-router'
import store from './store'

Vue.use(VueRouter)

function load (component) {
  return () => System.import(`components/${component}.vue`)
}

const router = new VueRouter({
  routes: [
    {
      path: '/login/:redirectTo',
      name: 'login',
      component: load('Auth/Login')
    }, // Default
    {
      path: '/',
      component: load('Layout'),
      meta: {
        isProtected: true
      },
      children: [
        {
          path: '',
          name: 'home',
          meta: {
            isProtected: true
          }
        }
      ]
    }, // Default
    {
      path: '*',
      component: load('Error404')
    } // Not found
  ]
})

router.beforeEach((to, from, next) => {
  if (to.name === 'login' && store.getters['auth/user']) {
    next({name: 'home'})
  }

  if (to.meta.isProtected && !store.getters['auth/user']) {
    next({name: 'login', params: {redirectTo: to.name}})
  } else {
    if (store.getters['auth/user'] && to.name === 'login') {
      next({name: 'home'})
    }
    next()
  }
})

export default router
