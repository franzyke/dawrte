import firebase from 'firebase'
import store from './store'

import config from './firebase.config.js'

firebase.initializeApp(config)

export const authCheck = () => {
  firebase.auth().onAuthStateChanged(function (user) {
    if (user) {
      store.commit('auth/setUser', user)
    }
    store.commit('auth/setBusy', false)
  })
}

export default firebase
